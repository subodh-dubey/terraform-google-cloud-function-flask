provider "google" {
  version = "3.5.0"

  credentials = file("admin.json") ## credentials json file path

  project = "test-terraform-275919"
  region  = "us-central1"
  zone    = "us-central1-c"
}

# zip up our source code
data "archive_file" "flask_app_zip" {
 type        = "zip"
 source_dir  = "${path.root}/flask_app/"
 output_path = "${path.root}/flask_app.zip"
}

# create the storage bucket
resource "google_storage_bucket" "flask_app_bucket" {
 name   = "flask_app_bucket"
}

# place the zip-ed code in the bucket
resource "google_storage_bucket_object" "flask_app_zip" {
 name   = "flask_app.zip"
 bucket = "${google_storage_bucket.flask_app_bucket.name}"
 source = "${path.root}/flask_app.zip"
}

resource "google_cloudfunctions_function" "flask_app_function" {
 name                  = "flask_app_function"
 description           = "Scheduled Hello World Flask App Function"
 available_memory_mb   = 256
 source_archive_bucket = "${google_storage_bucket.flask_app_bucket.name}"
 source_archive_object = "${google_storage_bucket_object.flask_app_zip.name}"
 timeout               = 60
 entry_point           = "hello_http"
 trigger_http          = true
 runtime               = "python37"
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.flask_app_function.project
  region         = google_cloudfunctions_function.flask_app_function.region
  cloud_function = google_cloudfunctions_function.flask_app_function.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
 }
